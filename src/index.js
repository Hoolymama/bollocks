/* retrns null if set intersection contains less than threshold members*/

var Sentencer = require('sentencer');

const verbPresentList = [ "awake", "be", "beat", "become", "begin", "bend", "bet", "bid", "bite", "blow", "break", "bring", "broadcast", "build", "burn", "buy", "catch", "choose", "come", "cost", "cut", "dig", "do", "draw", "dream", "drive", "drink", "eat", "fall", "feel", "fight", "find", "fly", "forget", "forgive", "freeze", "get", "give", "go", "grow", "hang", "have", "hear", "hide", "hit", "hold", "hurt", "keep", "know", "lay", "lead", "learn", "leave", "lend", "let", "lie", "lose", "make", "mean", "meet", "pay", "put down", "read", "ride", "ring", "rise", "run", "say", "see", "sell", "send", "show", "shut", "sing", "sink", "sit", "sleep", "speak", "spend", "stand", "stink", "swim", "take", "teach", "tear", "tell", "think", "throw", "understand", "wake", "wear", "win", "write"]
const verbPastList = [ "awoke", "was", "beat", "became", "began", "bent", "bet", "bid", "bit", "blew", "broke", "brought", "broadcast", "built", "burnt",  "bought", "caught", "chose", "came", "cost", "cut", "dug", "did", "drew", "dreamt", "drove", "drank", "ate", "fell", "felt", "fought", "found", "flew", "forgot", "forgave", "froze", "got", "gave", "went", "grew", "hung", "had", "heard", "hid", "hit", "held", "hurt", "kept", "knew", "laid", "led", "learned",  "left", "lent", "let", "lay", "lost", "made", "meant", "met", "paid", "put down", "read", "rode", "rang", "rose", "ran", "said", "saw", "sold", "sent", "showed",  "shut", "sang", "sank", "sat", "slept", "spoke", "spent", "stood", "stank", "swam", "took", "taught", "tore", "told", "thought", "threw", "understood", "woke", "wore", "won", "wrote"]
const verbParticipleList = [ "awoken", "been", "beaten", "become", "begun", "bent", "bet", "bid", "bitten", "blown", "broken", "brought", "broadcast", "built", "burnt", "bought", "caught", "chosen", "come", "cost", "cut", "dug", "done", "drawn", "dreamt", "driven", "drunk", "eaten", "fallen", "felt", "fought", "found", "flown", "forgotten", "forgiven", "frozen", "gotten", "given", "gone", "grown", "hung", "had", "heard", "hidden", "hit", "held", "hurt", "kept", "known", "laid", "led", "learnt", "left", "lent", "let", "lain", "lost", "made", "meant", "met", "paid", "put down", "read", "ridden", "rung", "risen", "run", "said", "seen", "sold", "sent", "shown", "shut", "sung", "sunk", "sat", "slept", "spoken", "spent", "stood", "stunk", "swum", "taken", "taught", "torn", "told", "thought", "thrown", "understood", "woken", "worn", "won", "written" ]
const timeList = [ "Monday", "Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday", "weekend", "week", "summer", "winter", "fall", "spring", "time", "decade"]
const eventList = ["meet-up", "bike race", "game", "fight", "dance", "pub crawl", "birthday party", "football match","conference","flight","book signing","gig","concert","festival","screening","sale","swim","carnival"]
const pronounList = ["he", "she", "they", "it", "we", "I", "you"] 
const pronounWasList = ["he was", "she was", "they were", "it was", "we were", "I was", "you were"] 
const pronounHasList = ["he has", "she has", "they have", "it has", "we have", "I have", "you have"] 
const pronounThinkList = ["he thinks","she thinks","they think","it thinks","we think","I think","you think","he reckons","she reckons","they reckon","it reckons","we reckon","I reckon","you reckon","he reckons","she reckons","they reckon","it reckons","we reckon","I reckon","you reckon","he feels","she feels","they feel","it feels","we feel","I feel","you feel","he guesses","she guesses","they guess","it guesses","we guess","I guess","you guess","he predicts","she predicts","they predict","it predicts","we predict","I predict","you predict","he supposes","she supposes","they suppose","it supposes","we suppose","I suppose","you suppose"] 
const pronounActionList = [ "he needs to", "she needs to", "they need to", "it needs to", "we need to", "I need to", "you need to", "he wants to", "she wants to", "they want to", "it wants to", "we want to", "I want to", "you want to", "he would like to", "she would like to", "they would like to", "it would like to", "we would like to", "I would like to", "you would like to", "he cannot", "she cannot", "they cannot", "it cannot", "we cannot", "I cannot", "you cannot", "he might", "she might", "they might", "it might", "we might", "I might", "you might", "he has to", "she has to", "they have to", "it has to", "we have to", "I have to", "you have to"] 
const possessiveList = [ "my", "your", "their", "his", "her", "our", "its" ]
const eventAdjList = [ "big", "small", "wild","tame", "quiet", "noisy", "open", "closed","important", "grand","impressive" ]
const placeList = [ "shop", "hotel", "house","building", "field", "road", "place", "tavern","restaurant", "hut","mansion" ]
const modifierList = ["anyway", "after all", "later", "earlier", "in the end", "beforehand", "for her", "for him", "gingerly", "openly", "furiously", "fast", "slowly"]
 
Sentencer.configure({
  actions: {
  	number: function(){
  		return Math.floor( Math.random() * 100 ) + 2;
  	},
  	verb_present: function(){
  		return  verbPresentList[Math.floor(Math.random()*verbPresentList.length)];
  	},
  	verb_past: function(){
  		return  verbPastList[Math.floor(Math.random()*verbPastList.length)];
  	},
  	verb_participle: function(){
  		return  verbParticipleList[Math.floor(Math.random()*verbParticipleList.length)];
  	} ,
  	pronoun: function(){
  		return  pronounList[Math.floor(Math.random()*pronounList.length)];
  	},
  	pronoun_was: function(){
  		return  pronounWasList[Math.floor(Math.random()*pronounWasList.length)];
  	},
  		pronoun_has: function(){
  		return  pronounHasList[Math.floor(Math.random()*pronounHasList.length)];
  	},
  	pronoun_think: function(){
  		return  pronounThinkList[Math.floor(Math.random()*pronounThinkList.length)];
  	},
  	pronoun_action: function(){
  		return  pronounActionList[Math.floor(Math.random()*pronounActionList.length)];
  	},
  	time: function(){
  		return  timeList[Math.floor(Math.random()*timeList.length)];
  	},
  	event: function(){
  		return  eventList[Math.floor(Math.random()*eventList.length)];
  	},
  	possessive: function(){
  		return  possessiveList[Math.floor(Math.random()*possessiveList.length)];
  	},
  	event_adjective: function(){
  		return  eventAdjList[Math.floor(Math.random()*eventAdjList.length)];
  	},
    an_event_adjective: function(){
    	let str = eventAdjList[Math.floor(Math.random()*eventAdjList.length)];
    	if (["a","e","i","o","u"].indexOf(str[0]) > -1) {
    		return ("an "+str)
    	} 
  		return ("a "+str)
  	},
    place: function(){
    	return placeList[Math.floor(Math.random()*placeList.length)];
  	},
  	modifier: function(){
    	return modifierList[Math.floor(Math.random()*modifierList.length)];
  	},
  	verbing: function(){
  		let v = verbPresentList[Math.floor(Math.random()*verbPresentList.length)];
  		const last = v.length-1
  		if ((v.length > 3)  && (v[(last)]  === "e") ) {
  			v = (v.substring(0, last) + "ing")
  		} else {
  			v = (v+ "ing")
  		}
  		return v
    	// return modifierList[Math.floor(Math.random()*modifierList.length)];
  	},
  }
});
const templates = [
"{{pronoun_has}} {{ verb_participle }} {{ a_noun }} and {{ an_adjective }} {{ noun }}",
"{{pronoun_think}} {{pronoun}} can {{verb_present}} for {{ an_adjective }} {{ noun }}",
"{{pronoun_has}} {{number}} ways to {{verb_present}} {{a_noun}} by {{verbing}} {{nouns}}",
"{{an_event_adjective}} {{event}} next {{time}} at the {{ adjective }} {{ noun }} {{place}}",
"{{pronoun}} {{verb_past}} for {{a_noun}} last {{time}}",
"{{possessive}} {{ adjective }} {{ noun }} {{verb_past}} the {{ nouns }}",
"{{pronoun_action}} {{verb_present}} {{possessive}} {{noun}} with {{ an_adjective }} {{ noun }}",
"{{pronoun}} {{verb_past}} {{ a_noun }} {{number}} times last {{time}}",
"{{pronoun_was}} {{verb_participle}} {{ a_noun }} but will {{verb_present}} {{modifier}}",

]
const templatesLen = templates.length

exports.randomSentence = () => {
	let sentence =  Sentencer.make(templates[Math.floor(Math.random()*templatesLen)])
	sentence = sentence[0].toUpperCase() + sentence.substring(1)
	return sentence
}
